const nombre = document.getElementById("nombre");
const apellidos = document.getElementById("apellido");
const correo = document.getElementById("email");
const contrasenia1 = document.getElementById("password");
const contrasenia2 = document.getElementById("Repassword");
const terminosYcondiciones = document.getElementById("check");
const form = document.getElementById("form");
const listInputs = document.querySelectorAll(".form-group");





form.addEventListener("submit", (e) => {
  e.preventDefault();
  let condicion = validaciondatos();
  if (condicion) {
    enviarFormulario();
  }
});

  function validacionForm() {
    form.lastElementChild.innerHTML = "";
    let condicion = true;
    listInputs.forEach((element) => {
      element.lastElementChild.innerHTML = "";
    });
  
    if (nombre.value.length < 3 || nombre.value.trim() == "") {
      muestraError("nombre", "Nombre no valido*");
      condicion = false;
    }
    if (apellidos.value.length < 3 || apellidos.value.trim() == "") {
      muestraError("apellido", "Apellido no valido");
      condicion = false;
    }
    if (correo.value.length < 3 || correo.value.trim() == "") {
      muestraError("correo", "Correo no valido*");
      condicion = false;
    }

    if (contrasenia1.value.length < 3 || contrasenia1.value.trim() == "") {
      muestraError("contrasenia1", "Contraseña no valido*");
      condicion = false;
    }
    if (contrasenia2.value != contrasenia2.value) {
      muestraError("contrasenia2", "Contraseña error*");
      condicion = false;
    }
    return condicion;
  }
  function muestraError(claseInput, mensaje) {
    let elemento = document.querySelector(`.${claseInput}`);
    elemento.lastElementChild.innerHTML = mensaje;
}
function enviarFormulario(){
    formulario.reset();
    formulario.lastElementChild.innerHTML = "¡Exitoso!";
    window.location.href = "Home.html" ;
}


